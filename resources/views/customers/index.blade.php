@extends('layouts.app')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<h1>This is customers list</h1>
<a href="{{route('customers.create')}}">Create a new customer </a>
<ul>
{{-- הערה --}}

@if (Request::is('customers'))
  <h2><b><a href="{{action('CustomerController@mycustomers')}}">My customers</a></b></h2>
  @else
  <h2><a href="{{action('CustomerController@index')}}">Show All customers</a></h2>
@endif
 

    @foreach($customers as $customer)
    <li>
    @if ($customer->status)
    <font color="green">  name:{{$customer->name}} email:{{$customer->email}} phone:{{$customer->phone}}  </font>  
     
      <a href= "{{route('customers.edit', $customer->id )}}"> Edit this customer </a>
           
         @else
         @can('manager')
           <button style="text-decoration: underline" id ="{{$customer->id}}" value="0"> Deal closed</button>
           @endcan
           name:{{$customer->name}} email:{{$customer->email}} phone:{{$customer->phone}}
           <a href= "{{route('customers.edit', $customer->id )}}"> Edit this customer </a>
       @endif

      <form method = 'post' action="{{action('CustomerController@destroy', $customer->id)}}">
        
        @csrf
        @method('DELETE')
        <div class = "form-group">
            <input type ="submit" class = "form-control" name="submit" value ="Delete customer">
        </div>

        </form>
 
    </li>
    @endforeach
   
</ul>

<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('customers')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
</script>
@endsection