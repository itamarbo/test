<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                 'name' => 'Avi',
                 'email' => 'a@a.com',
                 'password' =>bcrypt('12345678'),
                 'role' =>'manager',
                 'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                 'name' => 'Beni',
                 'email' => 'b@b.com',
                 'password' =>bcrypt('12345678'),
                 'role' =>'salesrep',
                 'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                 'name' => 'Codi',
                 'email' => 'c@c.com',
                 'password' =>bcrypt('12345678'),
                 'role' =>'salesrep',
                 'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
    
}
