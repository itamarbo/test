<?php

use App\User;
use App\Customer;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers/create/{id}', function ($id) {
    if (Gate::denies('manager')) {
        abort(403,"delete");
    }
    
});


//Route::get('/customer', 'CustomerController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('customers', 'CustomerController')->middleware('auth');
Route::get('/mycustomers', 'CustomerController@mycustomers');